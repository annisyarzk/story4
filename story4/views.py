from django.shortcuts import render

def index(request):
	return render(request, 'Story4.html')

def photos(request):
	return render(request, 'Photosupdate.html')